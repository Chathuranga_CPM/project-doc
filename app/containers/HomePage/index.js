/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';

export default function HomePage(){
  return (
    <div>
      Home page
    </div>
  );
}